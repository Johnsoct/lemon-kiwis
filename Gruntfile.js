module.exports = function(grunt) {
  grunt.initConfig({
    watch: {
      html: {
        files: ['src/html/srcindex.html'],
        tasks: ['htmlmin']
      },
      css: {
        files: 'src/sass/**/*.sass',
        tasks: ['sass', 'postcss']
      },
      js: {
        files: 'src/scripts/*.js',
        tasks: ['concat', 'babel', 'uglify']
      }
    },
    htmlmin: {
      dist: {                                      // Target
        options: {                                 // Target options
          removeComments: true,
          collapseWhitespace: true
        },
        files: {                                   // Dictionary of files
          'index.html': 'src/html/srcindex.html'
        }
      }
    },
    sass: {
      dist: {
        options: {
          style: 'compressed',
          sourceMap: 'none'
        },
        files: [{
          src: ['src/sass/*.sass'],
          dest: 'dist/css/styles.css'
        }]
      }
    },
    postcss: {
      options: {
        map: false,
        processors: [
          require('pixrem')(),
          require('autoprefixer')({browsers:'last 2 versions'}),
          require('cssnano')()
        ]
      },
      dist: {
        src: 'dist/css/styles.css'
      }
    },
    concat: {
        dist: {
            src: ['src/scripts/nav.js'],
            dest: 'src/scripts/app.js'
        }
    },
    babel: {
        options: {
            sourceMap: true,
            presets: ['es2015']
        },
        dist: {
            files: {
                'dist/scripts/app.js': 'src/scripts/app.js'
            }
        }
    },
    uglify: {
      build: {
        src: ['dist/scripts/app.js'],
        dest: 'dist/scripts/app.min.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-sass');

  grunt.registerTask('default', ['concat', 'babel', 'uglify']);
  grunt.registerTask('w', ['watch']);


};
