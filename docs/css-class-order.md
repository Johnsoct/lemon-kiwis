# Rules for inline CSS Classes
1. Tool classes, such as 'flex-row' always go first. This lets us know right off the bat we are overriding something.
2. Typography classes, such as 'text-center' always go last.
3. Offset classes must follow all column classes.
