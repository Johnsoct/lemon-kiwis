# Notes
1. If only one column width desired regardless of resolution, use mobile column widths, such as `.row_mobile-12` for 100% width on all resolutions. Mobile column classes do not contain media queries regarding when they're effective.

2. Offset classes can be used with multiple column containers in a single row if: A) columns are mobile only classes; B) 'default_column-gutter' is applied to the first, and only the first, column container.
