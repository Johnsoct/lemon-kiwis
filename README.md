# Project Lemon Kiwis - A CSS (Sass) Framework
I've always used my own styles instead of relying on a framework I end up overwriting more than using by default. If I built a framework around the way I design and develop, and what I believe to be the best methods, I should find a drastic increase in getting shit done in a shorter amount of time than if I were to continue to create my styles from scratch each project.

## So, what is a CSS framework?
1. Most explanations are bullshit and will say something like, "help us approach and resolve new problems." Most of the time, articles or individuals will describe the reason why they use frameworks in a way that raises more questions than provides answers.
2. A framework can be the steel, wood, wiring, AC, and dry wall to your project, but it's not the furniture, drapes, or personal effects.

## Are there different types of CSS frameworks?
Yes, there are **simple** and **complete**. An example of a complete framework is Bootstrap, as it has styles for just about any possible element you could imagine. An example of a simple framework would be 1140, which is simple a grid system maxing out at 1140px.

Ideally, your custom framework would sit in between these two or your projects will all appear very similar, as most Bootstrap'd projects do.

## What should a framework be?
1. A framework should be a foundational solution for a project's specific needs. If your needs will evolve or change over time, you must predict what that evolution or what those changes will look like before creating your framework. In other words, if you're overwriting a lot of your framework in the future, you're ruining the point of using it.
2. A framework should be light. The less code the less maintenance the less time required to update and evolve your framework. This could vary depending on your design style or typical project needs, but a framework shouldn't lug you down.

## What design elements should a framework cover?
1. Grid
2. Typography
2. Spacing information (margins and padding)
2. Utilities (Ex. a class to negate the margin and padding of an element)
3. Accessibility solutions (handicap accessibility)
4. Responsiveness solutions (media queries)
5. Browser compatibility solutions (supporting older and diverse browsers)
6. A strict CSS class naming standard (BEM, SMACSS, or a custom solution)
7. JavaScript (if needed)
8. Documentation (get started, starter template, important globals, spacing information, typography explanation, etc.)
